var HttpClient = function () {
    this.get = function (aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function () {
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }
        anHttpRequest.open("GET", aUrl, true);
        anHttpRequest.send(null);
    }
}

var extractDataCurrentDay = function (response) {
    data = []
    var j = 0
    for(var i = 0; i < 200; i++){
        tempDate = moment().endOf('hour').add(1,'millisecond').subtract(2010-i*10, 'minutes').add(2, 'hours').valueOf()
        tempVal = 0   
        
        if(j < JSON.parse(response).data.length){
            if(JSON.parse(response).data[j]._id * 1000 == tempDate){                
                tempVal = JSON.parse(response).data[j].W_a
                j++
            }
        }
        data.push({
            x: tempDate,
            y: tempVal         
        })
    }
    return data

    // data = []
    // for (item in JSON.parse(response).data) {
    //     data.push({
    //         x: JSON.parse(response).data[item]._id * 1000,
    //         y: JSON.parse(response).data[item].W_a
    //     })
    // }
    // return data
}

var extractDataCurrentWeek = function (response) {
    data = []
    var j = 0
    for(var i = 0; i < 9; i++){
        tempDate = moment().startOf('day').subtract(8-i, 'days').add(2, 'hours').valueOf()
        tempVal = 0     
        
        if(JSON.parse(response).data[j]._id * 1000 == tempDate){
            tempVal = JSON.parse(response).data[j].W_c/60/1000
            j++
        }
        data.push({
            x: tempDate,
            y: tempVal         
        })
    }
    return data

}

var extractDataCumulativeSingle = function (response) {
    response = JSON.parse(response)
    response = {
        day: response[0].Usage.day.El,
        week: response[0].Usage.week.El,
        month: response[0].Usage.month.El
    }
    return response
}

var extractDataCumulative = function(response){
    R3data = extractDataCumulativeSingle(response.R3)
    R3PVdata = extractDataCumulativeSingle(response.R3PV)
    updateSavings(R3PVdata)
}

var extractDataCurrent = function(response){
    R3dataDay = extractDataCurrentDay(response.R3day)
    R3PVdataDay = extractDataCurrentDay(response.R3PVday)
    R3dataWeek = extractDataCurrentWeek(response.R3week)
    R3PVdataWeek = extractDataCurrentWeek(response.R3PVweek)
    // console.log(R3dataWeek);
    // console.log(R3PVdataWeek);
    
    
    updateCharts(R3dataDay, R3PVdataDay, R3dataWeek, R3PVdataWeek)
}

var numberOfHoursToday = function(){
    var runningHoursDay = (moment().valueOf() - moment().startOf('day').valueOf())/1000/3600
    return (runningHoursDay > 7) ? (runningHoursDay - 7) : 0
}

var numberOfHoursWeek = function(){
    numDaysInWeek = moment().isoWeekday()
    return (numDaysInWeek < 6) ? ((numDaysInWeek - 1) * 8 + numberOfHoursToday()) : (5 * 8)
}

var numberOfHoursMonth = function(){
    var dayCounter = 0
    var startDate = moment().startOf('month')
    while(startDate.date() != moment().date()){
        if(startDate.isoWeekday() < 6){
            dayCounter++
        }
        startDate = startDate.add(1, 'days')
    }
    return dayCounter*8 + numberOfHoursToday()
}

var updateSavings = function (R3PVdata) {

    // PV savings
    document.getElementById("PVdaySavingsE").innerText = (R3PVdata.day / 1000).toFixed(2)
    document.getElementById("PVweekSavingsE").innerText = (R3PVdata.week / 1000).toFixed(2)
    document.getElementById("PVmonthSavingsE").innerText = (R3PVdata.month / 1000).toFixed(2)

    document.getElementById("PVdaySavingsC").innerText = (R3PVdata.day / 1000 * 0.93).toFixed(2)
    document.getElementById("PVweekSavingsC").innerText = (R3PVdata.week / 1000 * 0.93).toFixed(2)
    document.getElementById("PVmonthSavingsC").innerText = (R3PVdata.month / 1000 * 0.93).toFixed(2)

    document.getElementById("PVdaySavingsR").innerText = (R3PVdata.day / 1000 * 1.8).toFixed(2)
    document.getElementById("PVweekSavingsR").innerText = (R3PVdata.week / 1000 * 1.8).toFixed(2)
    document.getElementById("PVmonthSavingsR").innerText = (R3PVdata.month / 1000 * 1.8).toFixed(2)
    
    // LED savings
    var powerSaved_kW = (8*58 - 4*60) / 1000

    var LEDkWhSavedToday = numberOfHoursToday() * powerSaved_kW // Today

    var LEDkWhSavedWeek = numberOfHoursWeek() * powerSaved_kW // This week

    var LEDkWhSavedMonth = numberOfHoursMonth() * powerSaved_kW // This month

    document.getElementById("LEDdaySavingsE").innerText = LEDkWhSavedToday.toFixed(2)
    document.getElementById("LEDweekSavingsE").innerText = LEDkWhSavedWeek.toFixed(2)
    document.getElementById("LEDmonthSavingsE").innerText = LEDkWhSavedMonth.toFixed(2)

    document.getElementById("LEDdaySavingsC").innerText = (LEDkWhSavedToday * 0.93).toFixed(2)
    document.getElementById("LEDweekSavingsC").innerText = (LEDkWhSavedWeek * 0.93).toFixed(2)
    document.getElementById("LEDmonthSavingsC").innerText = (LEDkWhSavedMonth * 0.93).toFixed(2)

    document.getElementById("LEDdaySavingsR").innerText = (LEDkWhSavedToday * 1.8).toFixed(2)
    document.getElementById("LEDweekSavingsR").innerText = (LEDkWhSavedWeek * 1.8).toFixed(2)
    document.getElementById("LEDmonthSavingsR").innerText = (LEDkWhSavedMonth * 1.8).toFixed(2)
}

var updateCharts = function (R3dataDay, R3PVdataDay, R3dataWeek, R3PVdataWeek) {
    // Update Grade R3 chart
    var ctx = document.getElementById("chartR3day");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            datasets: [{
                label: 'Load',
                data: R3dataDay,
                lineTension: 0,
                backgroundColor: 'orange',
                borderColor: 'orange',
                borderWidth: 1,
                pointRadius: 1,
                pointBackgroundColor: 'orange',
                fill: false
            },
            {
                label: 'PV Generation',
                data: R3PVdataDay,
                lineTension: 0,
                backgroundColor: '#00FF00',
                borderColor: '#00FF00',
                borderWidth: 1,
                pointRadius: 1,
                pointBackgroundColor: '#00FF00',
                fill: false
            }
            ]
        },
        options: {
            maintainAspectRatio: false,
            responsive: true,
            title: {
                display: true,
                text: "Classroom last 24 hours"
            },
            scales: {
                xAxes: [{
                    barPercentage: 0.2,
                    type: "time",
                    time: {
                        min: moment().subtract(1, 'day').valueOf(),
                        max: moment().valueOf(),
                        format: 'DD/MM/YYYY',
                        tooltipFormat: 'll'
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Time'
                    }
                }],
                yAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Power usage [W]'
                    },
                    ticks: {
                        beginAtZero: true,
                        steps: 40,
                        stepValue: 100,
                        max: 3500
                    }
                }]
            }
        }
    });
    var ctx = document.getElementById("chartR3week");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            datasets: [{
                label: 'Load',
                data: R3dataWeek,
                lineTension: 0,
                backgroundColor: 'orange',
                borderColor: 'orange',
                borderWidth: 1,
                pointRadius: 1,
                pointBackgroundColor: 'orange',
                fill: false
            },
            {
                label: 'PV Generation',
                data: R3PVdataWeek,
                lineTension: 0,
                backgroundColor: '#00FF00',
                borderColor: '#00FF00',
                borderWidth: 1,
                pointRadius: 1,
                pointBackgroundColor: '#00FF00',
                fill: false
            }
            ]
        },
        options: {
            maintainAspectRatio: false,
            responsive: true,
            title: {
                display: true,
                text: "Classroom last week"
            },
            scales: {
                xAxes: [{
                    type: "time",
                    time: {
                        min: moment().startOf('day').subtract(1, 'week').valueOf(),
                        max: moment().endOf('day').valueOf(),
                        unit: 'day',
                        format: 'DD/MM/YYYY',
                        tooltipFormat: 'll'
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Time'
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Energy usage [kWh]'
                    },
                    ticks: {
                        beginAtZero: true,
                        steps: 15,
                        stepValue: 1,
                        max: 15
                    }
                }]
            }
        }
    });
}

var getCurrentData = function () {
    console.log(new Date(Date.now()).toLocaleString());

    endTime = Date.now() / 1000
    startTimeDay = endTime - 24 * 60 * 60
    startTimeWeek = endTime - 7 * 24 * 60 * 60

    sampleRateDay = 60 * 10
    sampleRateWeek = 60 * 60 * 24

    var client = new HttpClient();
    var response = {}

    client.get('https://greenclassroom.herokuapp.com/api/instantaneous?deviceID=ztp47zLXY6Ed2qKdE&startTime=' + startTimeDay + '&endTime=' + endTime + '&sampleRate=' + sampleRateDay, function (responseR3day) {
        response.R3day = responseR3day
        if(Object.keys(response).length == 4) extractDataCurrent(response)
    })
    client.get('https://greenclassroom.herokuapp.com/api/instantaneous?deviceID=5aiKhfQjieRz3mHEs&startTime=' + startTimeDay + '&endTime=' + endTime + '&sampleRate=' + sampleRateDay, function (responseR3PVday) {
        response.R3PVday = responseR3PVday
        if(Object.keys(response).length == 4) extractDataCurrent(response)
    })
    client.get('https://greenclassroom.herokuapp.com/api/instantaneous?deviceID=ztp47zLXY6Ed2qKdE&startTime=' + startTimeWeek + '&endTime=' + endTime + '&sampleRate=' + sampleRateWeek, function (responseR3week) {
        response.R3week = responseR3week
        if(Object.keys(response).length == 4) extractDataCurrent(response)
    })
    client.get('https://greenclassroom.herokuapp.com/api/instantaneous?deviceID=5aiKhfQjieRz3mHEs&startTime=' + startTimeWeek + '&endTime=' + endTime + '&sampleRate=' + sampleRateWeek, function (responseR3PVweek) {
        response.R3PVweek = responseR3PVweek
        if(Object.keys(response).length == 4) extractDataCurrent(response)
    })
}

var getCumulativeData = function () {
    var client = new HttpClient();
    var response = {}
    client.get('https://greenclassroom.herokuapp.com/api/cumulative?deviceID=ztp47zLXY6Ed2qKdE', function (responseR3) {
        response.R3 = responseR3
        if(Object.keys(response).length == 2) extractDataCumulative(response)
    })
    client.get('https://greenclassroom.herokuapp.com/api/cumulative?deviceID=5aiKhfQjieRz3mHEs', function (responseR3PV) {
        response.R3PV = responseR3PV
        if(Object.keys(response).length == 2) extractDataCumulative(response)
    })
}

var getData = function () {
    getCurrentData()
    getCumulativeData()
}

getData()
setInterval(getData, 1000 * 60 * 30)